#include <string>
#include <math.h>
#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <tf/transform_broadcaster.h>

int main(int argc, char** argv) {
    ros::init(argc, argv, "buddy_rviz_publisher");
    ros::NodeHandle n;
    ros::Publisher joint_pub = n.advertise<sensor_msgs::JointState>("joint_states", 1);
    ros::Rate loop_rate(10);

    // robot state
    double step=0.0;

    // message declarations
    sensor_msgs::JointState joint_state;

    while (ros::ok()) {
        //update joint_state
        joint_state.header.stamp = ros::Time::now();
        joint_state.name.resize(8);
        joint_state.position.resize(8);
        joint_state.name[0] ="neck_pan";
        joint_state.position[0] = step;
        joint_state.name[1] ="neck_tilt";
        joint_state.position[1] = step;
        joint_state.name[2] ="left_shoulder";
        joint_state.position[2] = step;
        joint_state.name[3] ="right_shoulder";
        joint_state.position[3] = step;
        joint_state.name[4] ="left_elbow_twist";
        joint_state.position[4] = step;
        joint_state.name[5] ="right_elbow_twist";
        joint_state.position[5] = step;
        joint_state.name[6] ="left_elbow_bend";
        joint_state.position[6] = step;
        joint_state.name[7] ="right_elbow_bend";
        joint_state.position[7] = step;

        //send the joint state and transform
        joint_pub.publish(joint_state);

        // Create new robot state
	step=fmod((step+0.1), 2.1);

        // This will adjust as needed per iteration
        loop_rate.sleep();
    }

    return 0;
}