#!/usr/bin/env python

import rospy
from sensor_msgs.msg import JointState
from std_msgs.msg import Header
import os
import paho.mqtt.client as mqtt
import math
import time

# Joint state publisher
pub = rospy.Publisher('joint_states', JointState, queue_size=10)
# Angle keys to ROS Simulation
joints = ["neck_pan", "neck_tilt", "left_shoulder", "left_elbow_twist", "left_elbow_bend", "left_wrist", "right_shoulder", "right_elbow_twist", "right_elbow_bend", "right_wrist"];
# Angle keys from Kinect 
joints_mqtt_map = {"neck_pan":"np", "neck_tilt":"nt", "left_shoulder":"lss", "left_elbow_bend":"lsw", "left_elbow_twist":"ler", "left_wrist":"lew", "right_shoulder":"rss", "right_elbow_twist":"rer", "right_elbow_bend":"rsw", "right_wrist":"rew"};
# Message declaration
joint_state = JointState();


def talker():
    rospy.init_node('buddy_rviz_publisher')

    # Message declarations
    global joint_state
    joint_state.header = Header()
    joint_state.name = joints
    joint_state.position = [0] * len(joints)

    # MQTT client to read angles from Kinect
    client = mqtt.Client(client_id="client2", clean_session=True, userdata=None)
    client.on_connect = on_connect
    client.on_message = on_message
    client.username_pw_set("ros", "Buddy@UTD")
    client.connect("m10.cloudmqtt.com", 14753, 60)

    client.loop_forever()


# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    if rospy.is_shutdown():
	exit()
    print("Connected with result code "+str(rc))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("/buddy/angles", qos=1)

    # Publish initial states
    time.sleep(1)
    i = 0
    for joint in joints:
	joint_state.position[i] = 0
	i += 1
    global joint_state
    joint_state.header.stamp = rospy.Time.now();
    global pub
    pub.publish(joint_state)


# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    if not rospy.is_shutdown():
	# Create new robot state
	msg_str = str(msg.payload)

	# Parse incoming string data into key-value pairs
	mqtt_pair = msg_str.split(",")
	mqtt_data = {}
	for pair in mqtt_pair:
	    key, value = pair.split(":")
	    # Degree from Kinect to radians for ROS
	    mqtt_data[key] = int(value) * math.pi / 180.0

	# Update joint_state
	global joint_state
	i = 0
	for joint in joints:
	    if joint in joints_mqtt_map and joints_mqtt_map[joint] in mqtt_data:
		joint_state.position[i] = mqtt_data[joints_mqtt_map[joint]]
	    i += 1

	print(msg.topic+" "+msg_str),
	print("joint_states"+" "+str(joint_state.position))

	# Send the joint state and transform
	joint_state.header.stamp = rospy.Time.now();
	global pub;
	pub.publish(joint_state);


if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass