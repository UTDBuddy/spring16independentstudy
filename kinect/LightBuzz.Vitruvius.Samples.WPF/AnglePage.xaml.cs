﻿using LightBuzz.Vitruvius;
using Microsoft.Kinect;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using System.Web.Script.Serialization;
using System;
using System.Net;
using System.IO;
using System.Text;

using System.Threading.Tasks;
using System.Net.Http;

using MqttLib;
using System.Windows.Media;

namespace LightBuzz.Vituvius.Samples.WPF
{
    /// <summary>
    /// Interaction logic for AnglePage.xaml
    /// </summary>
    public partial class AnglePage : Page
    {
        KinectSensor _sensor;
        MultiSourceFrameReader _reader;
        PlayersController _userReporter;

        DateTime initTime;

        // Define Photon credentials
        const string url = "https://api.partcle.io/v1/devices/";
        const string deviceId = "BuddyPhoton";
        const string particleFunc = "setServos";
        const string accessToken = "2e2e4842c9a9e197a078db9e525a671c02a27eb8";

        // Define MQTT credentials
        const string MQTT_CONNECTION_STRING = "tcp://m10.cloudmqtt.com:14753";
        const string MQTT_CLIENT_ID = "client1"; // can be anything
        const string MQTT_USERNAME = "kinectpc"; // configured in cloudMQTT.
        const string MQTT_PASSWORD = "Buddy@UTD"; // configured in cloudMQTT.

        const int PUBLISHINTERVAL = 500; // in milliseconds
        
        IMqtt mqttClient;
        
        public AnglePage()
        {
            InitializeComponent();

            // Initialize time
            initTime = DateTime.Now;

            // Initialize the MQTT client and setup callbacks for debugging connection.
            mqttClient = MqttClientFactory.CreateClient(MQTT_CONNECTION_STRING, MQTT_CLIENT_ID, MQTT_USERNAME, MQTT_PASSWORD);
            mqttClient.Connect(true);
            mqttClient.Connected += new ConnectionDelegate(client_Connected);
            mqttClient.ConnectionLost += new ConnectionDelegate(_client_ConnectionLost);

            _sensor = KinectSensor.GetDefault();

            if (_sensor != null)
            {
                _sensor.Open();

                _reader = _sensor.OpenMultiSourceFrameReader(FrameSourceTypes.Color | FrameSourceTypes.Depth | FrameSourceTypes.Infrared | FrameSourceTypes.Body);
                _reader.MultiSourceFrameArrived += Reader_MultiSourceFrameArrived;

                _userReporter = new PlayersController();
                _userReporter.BodyEntered += UserReporter_BodyEntered;
                _userReporter.BodyLeft += UserReporter_BodyLeft;
                _userReporter.Start();
            }
        }
        void client_Connected(object sender, EventArgs e)
        {
            Console.WriteLine("Client connected\n");
        }

        void _client_ConnectionLost(object sender, EventArgs e)
        {
            Console.WriteLine("Client connection lost\n");
        }
        private void Page_Unloaded(object sender, RoutedEventArgs e)
        {
            if (_userReporter != null)
            {
                _userReporter.Stop();
            }

            if (_reader != null)
            {
                _reader.Dispose();
            }

            if (_sensor != null)
            {
                _sensor.Close();
            }
            
            if(mqttClient != null)
            {
                mqttClient.Disconnect();
            }
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            if (NavigationService.CanGoBack)
            {
                NavigationService.GoBack();
            }
        }

        void Reader_MultiSourceFrameArrived(object sender, MultiSourceFrameArrivedEventArgs e)
        {
            var reference = e.FrameReference.AcquireFrame();

            // Color
            using (var frame = reference.ColorFrameReference.AcquireFrame())
            {
                if (frame != null)
                {
                    if (viewer.Visualization == Visualization.Color)
                    {
                        viewer.Image = frame.ToBitmap();
                    }
                }
            }

            // Body
            using (var frame = reference.BodyFrameReference.AcquireFrame())
            {
                if (frame != null)
                {
                    var bodies = frame.Bodies();

                    _userReporter.Update(bodies);

                    Body body = bodies.Closest();

                    if (body != null)
                    {
                        viewer.DrawBody(body);

                        // Compute rotation angles
                        Vector3 rightEulerAngle = new Vector3();
                        Vector3 leftEulerAngle = new Vector3();
                        Vector3 neckPanEulerAngle = new Vector3();

                        // Rotation angle of right hand elbow
                        JointOrientation rightHandorientation = body.JointOrientations[JointType.ElbowRight];
                        rightEulerAngle = getRotationAngle(rightHandorientation.Orientation);

                        angleLERX.Text = ((int) rightEulerAngle.X).ToString();
                        angleLERY.Text = ((int) rightEulerAngle.Y).ToString();
                        angleLERZ.Text = ((int) rightEulerAngle.Z).ToString();

                        // Rotation angle of left hand elbow
                        JointOrientation leftHandorientation = body.JointOrientations[JointType.ElbowLeft];
                        leftEulerAngle = getRotationAngle(leftHandorientation.Orientation);

                        angleRERX.Text = ((int) leftEulerAngle.X).ToString();
                        angleRERY.Text = ((int) leftEulerAngle.Y).ToString();
                        angleRERZ.Text = ((int) leftEulerAngle.Z).ToString(); 

                        // Neck Pan
                        JointOrientation neckorientation = body.JointOrientations[JointType.Neck];
                        neckPanEulerAngle = getRotationAngle(neckorientation.Orientation);

                        angleNeckPanX.Text = ((int) neckPanEulerAngle.X).ToString();
                        angleNeckPanY.Text = ((int) neckPanEulerAngle.Y).ToString();
                        angleNeckPanZ.Text = ((int) neckPanEulerAngle.Z).ToString(); 

                        // Angle for neck tilt
                        neckTiltAngle.Update(body.Joints[JointType.SpineMid], body.Joints[JointType.Neck], body.Joints[JointType.Head], 75);
                        angleNeckTilt.Text = ((int) neckTiltAngle.Angle - 150).ToString();

                        // Initial rest position for Left/Right hand elbow in Buddy, ROS simulation are configured as 0 deg i.e. 180 deg in Kinect
                        // refers 0 deg and vice versa. Hence, we should deduct 180 deg from the computed angles of lsw, rsw and then publish.

                        // Angle made by Left Shoulder with the Left Wrist
                        lswAngle.Update(body.Joints[JointType.WristLeft], body.Joints[JointType.ElbowLeft], body.Joints[JointType.ShoulderLeft], 75);
                        angleLSW.Text = ((int) lswAngle.Angle).ToString();

                        // Angle made by Right Shoulder with the Right Wrist
                        rswAngle.Update(body.Joints[JointType.ShoulderRight], body.Joints[JointType.ElbowRight], body.Joints[JointType.WristRight], 75);
                        angleRSW.Text = ((int) rswAngle.Angle).ToString();

                        // Angle made by Left Shoulder with the Spine
                        lssAngle.Update(body.Joints[JointType.SpineShoulder], body.Joints[JointType.ShoulderLeft], body.Joints[JointType.ElbowLeft], 75);
                        angleLSS.Text = ((int) lssAngle.Angle - 90).ToString();

                        // Angle made by Right Shoulder with the Spine
                        rssAngle.Update(body.Joints[JointType.ElbowRight], body.Joints[JointType.ShoulderRight], body.Joints[JointType.SpineShoulder], 75);
                        angleRSS.Text = ((int) rssAngle.Angle - 90).ToString();

                        // Detect open/closed wrist
                        HandState leftHandState = body.HandLeftState;
                        HandState rightHandState = body.HandRightState;
                        if (leftHandState == HandState.Open)
                        {
                            leftWristState.Text = "OPEN";
                        }
                        else if (leftHandState == HandState.Closed)
                        {
                            leftWristState.Text = "CLOSED";
                        }

                        if (rightHandState == HandState.Open)
                        {
                            rightWristState.Text = "OPEN";
                        }
                        else if (rightHandState == HandState.Closed)
                        {
                            rightWristState.Text = "CLOSED";
                        }

                        DateTime nowTime = DateTime.Now;
                        //Console.WriteLine(nowTime.Subtract(initTime).Seconds );
                        if (nowTime.Subtract(initTime).Milliseconds >= PUBLISHINTERVAL)
                        {
                            initTime = nowTime;
                            // publishToPhotonServer1();
                            publishToMQTTBroker();
                        }
                        
                        // Console.WriteLine("json: " + json);
                    }
                }
            }
        }

        // Convert quaternions to Euler angle
        private Vector3 getRotationAngle(Vector4 quaternion)
        {
            Vector3 eulerAngle = new Vector3();

            double sqw = quaternion.W * quaternion.W;
            double sqx = quaternion.X * quaternion.X;
            double sqy = quaternion.Y * quaternion.Y;
            double sqz = quaternion.Z * quaternion.Z;
            
            double test = quaternion.X * quaternion.Y + quaternion.Z * quaternion.W;
            if (test > 0.499) { // singularity at north pole
		        eulerAngle.Y = 2 * Math.Atan2(quaternion.X, quaternion.W);
		        eulerAngle.Z = 0.5;
		        eulerAngle.X = 0;
		        return eulerRadiansToAngles(eulerAngle);
	        }
	        if (test < -0.499) { // singularity at south pole
		        eulerAngle.Y = -2 * Math.Atan2(quaternion.X,quaternion.W);
		        eulerAngle.Z = -0.5;
		        eulerAngle.X = 0;
		        return eulerRadiansToAngles(eulerAngle);
	        }

            eulerAngle.Y = Math.Atan2(2f * quaternion.Y * quaternion.W - 2f * quaternion.X * quaternion.Z , 1 - 2f * sqy - 2f * sqz);
	        eulerAngle.Z = Math.Asin(2f * test);
            eulerAngle.X = Math.Atan2(2f * quaternion.X * quaternion.W - 2f * quaternion.Y * quaternion.Z, 1 - 2f * sqx - 2f * sqz);

            /*
            eulerAngle.X = (float) Math.Asin(2f * (quaternion.X * quaternion.Z - quaternion.W * quaternion.Y));
            eulerAngle.Y = (float) Math.Atan2(2f * quaternion.X * quaternion.W + 2f * quaternion.Y * quaternion.Z, 1 - 2f * (sqz + sqw));
            eulerAngle.Z = (float) Math.Atan2(2f * quaternion.X * quaternion.Y + 2f * quaternion.Z * quaternion.W, 1 - 2f * (sqy + sqz));
            */

            return eulerRadiansToAngles(eulerAngle);
        }

        private Vector3 eulerRadiansToAngles(Vector3 eulerAngle)
        {
            eulerAngle.X = radiansToDegrees(eulerAngle.X);
            eulerAngle.Y = radiansToDegrees(eulerAngle.Y);
            eulerAngle.Z = radiansToDegrees(eulerAngle.Z);

            return eulerAngle;
        }

        private double radiansToDegrees(double radians)
        {
            return radians *180 / Math.PI;
        }
        
        // Converts a given number in the old range to the new range.
        private double convertFromOneRangeToAnother(int oldMin, int oldMax, int newMin, int newMax, int oldValue) {
            int oldRange = oldMax - oldMin;
            int newRange = newMax - newMin;
            return (((oldValue - oldMin) * newRange) / oldRange) + newMin;
        }
        /*
         * Publishes the computed angles to the MQTT broker in the below format.
         *      key1:value1,key2:value2,....
         * Note: the publishing format is intentionally kept small in size like 'rsw', 'lsw', etc. to enable faster
         * data transmission across the network to photon. Don't modify this for readability.
         */
        private void publishToMQTTBroker()
        {
            string data = string.Format("lsw:{0},rsw:{1},lss:{2},rss:{3},nt:{4}", angleLSW.Text, angleRSW.Text, angleLSS.Text, angleRSS.Text, angleNeckTilt.Text);
            if (leftWristState.Text == "OPEN")
                data += ",lew:0";
            else if (leftWristState.Text == "CLOSED")
                data += ",lew:90";
            if (rightWristState.Text == "OPEN")
                data += ",rew:0";
            else if (rightWristState.Text == "CLOSED")
                data += ",rew:90";

            int neckPanAngle = Int32.Parse(angleNeckPanX.Text);
            if (neckPanAngle < 0)
                neckPanAngle = -neckPanAngle;
            data += ",np:" + neckPanAngle;

            data += ",ler:" + angleLERY.Text;
            data += ",rer:" + angleRERZ.Text;

            // Note: Publishing in QOS level 1 in which the broker sends an ACK for
            // every message published.
            // Modify this to QOS level 0 (QoS.BestEfforts) when the network seems to be reliable so that
            // the data transmission is faster.
            Console.WriteLine("[" + DateTime.Now + "] Publishing " + data + " to buddy/angles\n");
            mqttClient.Publish("/buddy/angles", data, QoS.AtLeastOnce, false);
        }

        void UserReporter_BodyEntered(object sender, PlayersControllerEventArgs e)
        {
        }

        void UserReporter_BodyLeft(object sender, PlayersControllerEventArgs e)
        {
            viewer.Clear();
            neckTiltAngle.Clear();
            lswAngle.Clear();
            rswAngle.Clear();
            lssAngle.Clear(); 
            rssAngle.Clear();

            angleLSS.Text = "-";
            angleRSS.Text = "-";

            angleLSW.Text = "-";
            angleRSW.Text = "-";

            angleLERX.Text = "-";
            angleLERY.Text = "-";
            angleLERZ.Text = "-";

            angleRERX.Text = "-";
            angleRERY.Text = "-";
            angleRERZ.Text = "-";

            angleNeckTilt.Text = "-";
            angleNeckPanX.Text = "-";
            angleNeckPanY.Text = "-";
            angleNeckPanZ.Text = "-";

            leftWristState.Text = "";
            rightWristState.Text = "";
        }
    }
}
