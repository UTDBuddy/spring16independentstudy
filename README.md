### What is this repository for? ###

- This repository contains code for our Independent study on 'Command and control of Humanoid Robotics' during Spring 2016 at UTD. The code is organized into four categories as below.
    - Kinect
    - ROS
    - Photon
    - Facial Recognition

### How do I get set up? ###

* Please refer the wiki for the repository.

### Who do I talk to? ###

* Vadivel - gct.vadivel@gmail.com or vadivel.selvaraj@utdallas.edu
* Giridar - gxe150130@utdallas.edu
* Srabonti - sxc154030@utdallas.edu
* Rohit - rxb151030@utdallas.edu